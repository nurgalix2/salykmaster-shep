package kz.salykmaster.shep.wservice.controllers.factory;

import kz.salykmaster.shep.sono.form91000v24.SonoField;
import kz.salykmaster.shep.sono.form91000v24.SonoSheet;
import kz.salykmaster.shep.sono.form91000v24.SonoSheetGroup;
import kz.salykmaster.shep.sono.form91000v24.SonoForm;
import kz.salykmaster.shep.sono.form91000v24.ObjectFactory;
import kz.salykmaster.shep.sono.form91000v24.SonoFno;

import java.time.LocalDate;

public class Form91000v24Factory {

    public static SonoFno createEmptySonoFno() {

        ObjectFactory objectFactory = new ObjectFactory();

        LocalDate today = LocalDate.now();

        SonoFno fno = objectFactory.createSonoFno();

        fno.setCode("910.00");
        fno.setFormatVersion("1");
        fno.setVersion("24");

        SonoForm form = objectFactory.createSonoForm();
        form.setName("form_910_00");
        fno.getForm().add(form);

        SonoSheetGroup sheetGroup = objectFactory.createSonoSheetGroup();
        form.setSheetGroup(sheetGroup);

        SonoSheet page_910_00_01 = objectFactory.createSonoSheet();
        page_910_00_01.setName("page_910_00_01");
        sheetGroup.getSheet().add(page_910_00_01);

        page_910_00_01.getField().add(createField("currency_code", null));
        page_910_00_01.getField().add(createField("field_910_00_003_a", null));
        page_910_00_01.getField().add(createField("ved_buch", null));
        page_910_00_01.getField().add(createField("dt_main", null));
        page_910_00_01.getField().add(createField("field_910_00_005", null));
        page_910_00_01.getField().add(createField("tis_number", null));
        page_910_00_01.getField().add(createField("uchr_dov", null));
        page_910_00_01.getField().add(createField("PaperFormsBarcode1", null));
        page_910_00_01.getField().add(createField("payer_name1", null));
        page_910_00_01.getField().add(createField("field_910_00_009", null));
        page_910_00_01.getField().add(createField("field_910_00_004", null));
        page_910_00_01.getField().add(createField("period_year", null));
        page_910_00_01.getField().add(createField("upr_dov", null));
        page_910_00_01.getField().add(createField("cb_n2", null));
        page_910_00_01.getField().add(createField("dt_w", null));
        page_910_00_01.getField().add(createField("field_910_00_001_A", null));
        page_910_00_01.getField().add(createField("field_910_00_003_b", null));
        page_910_00_01.getField().add(createField("rnn", null));
        page_910_00_01.getField().add(createField("field_910_00_006", null));
        page_910_00_01.getField().add(createField("field_910_00_007", null));
        page_910_00_01.getField().add(createField("field_910_00_011", null));
        page_910_00_01.getField().add(createField("field_910_00_010", null));
        page_910_00_01.getField().add(createField("dt_additional", null));
        page_910_00_01.getField().add(createField("notice_date", null));
        page_910_00_01.getField().add(createField("field_910_00_001_A_I", null));
        page_910_00_01.getField().add(createField("tis", null));
        page_910_00_01.getField().add(createField("dt_final", null));
        page_910_00_01.getField().add(createField("cb_n1", null));
        page_910_00_01.getField().add(createField("iin", null));
        page_910_00_01.getField().add(createField("tis_name2", null));
        page_910_00_01.getField().add(createField("field_910_00_003", null));
        page_910_00_01.getField().add(createField("dt_cb", null));
        page_910_00_01.getField().add(createField("payer_name2", null));
        page_910_00_01.getField().add(createField("field_910_00_001_B", null));
        page_910_00_01.getField().add(createField("field_910_00_001", null));
        page_910_00_01.getField().add(createField("tis_date", null));
        page_910_00_01.getField().add(createField("dt_regular", null));
        page_910_00_01.getField().add(createField("field_910_00_002", null));
        page_910_00_01.getField().add(createField("tis_name1", null));
        page_910_00_01.getField().add(createField("ne_ved_buch", null));
        page_910_00_01.getField().add(createField("field_910_00_001_B_I", null));
        page_910_00_01.getField().add(createField("payer_name3", null));
        page_910_00_01.getField().add(createField("notice_number", null));
        page_910_00_01.getField().add(createField("dt_notice", null));
        page_910_00_01.getField().add(createField("field_910_00_008", null));
        page_910_00_01.getField().add(createField("period_half_year", null));

        return null;
    }

    public static SonoField createField(String name, String value) {
        SonoField field = new SonoField();
        field.setName(name);
        field.setValue(value);
        return field;
    }

}
