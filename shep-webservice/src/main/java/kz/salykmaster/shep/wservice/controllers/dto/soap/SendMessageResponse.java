package kz.salykmaster.shep.wservice.controllers.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendMessageResponse {
    private MessageInfoResponse responseInfo;
    private ResponseData responseData;
}
