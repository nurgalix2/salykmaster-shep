package kz.salykmaster.shep.wservice.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import kz.salykmaster.shep.service.sync.SendMessageSendMessageFaultMsg;
import kz.salykmaster.shep.service.sync.SyncSendMessageResponse;
import kz.salykmaster.shep.wservice.client.SyncServiceClient;
import kz.salykmaster.shep.wservice.controllers.factory.Factory;
import kz.salykmaster.shep.wservice.payload.response.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Security;

@RestController
@RequestMapping("/api/v1/send")
@Api(tags = {"API для взаимодействие с ИС СОНО"})
public class SendController {

    static {
        KalkanProvider kalkanProvider = new KalkanProvider();
        Security.addProvider(kalkanProvider);
        KncaXS.loadXMLSecurity();
    }

    Logger logger = LoggerFactory.getLogger(SendController.class);

    @Autowired
    SyncServiceClient client;

    @ApiOperation(value = "Отправить запрос", response = ResponseHelper.class)
    @PostMapping(value = "/send")
    public ResponseHelper send(@ApiParam(name = "serviceId", value = "ИД сервиса",
            allowableValues = "SONO_FNO_SEND,SONO_FNO_GET_STATUS")
                               @RequestParam ServiceId serviceId,
                               @ApiParam(name = "signedXML", value = "Подписанная форма")
                               @RequestBody String signedXML) {
        try {

            signedXML = signedXML.replace("\\r\\n", System.lineSeparator()).replace("\\", "");

            SyncSendMessageResponse response = client.sendMessage(serviceId.name(), signedXML);

            return new ResponseHelper(Factory.convertToDto(response));

        } catch (SendMessageSendMessageFaultMsg error) {
            error.printStackTrace();
            return new ResponseHelper(error);
        } catch (Exception error) {
            error.printStackTrace();
            return new ResponseHelper(error);
        }

    }

    enum ServiceId {
        SONO_FNO_SEND,
        SONO_FNO_GET_STATUS
    }

}
