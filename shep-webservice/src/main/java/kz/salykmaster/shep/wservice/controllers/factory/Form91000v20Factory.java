package kz.salykmaster.shep.wservice.controllers.factory;

import kz.salykmaster.shep.sono.form91000v20.*;
import kz.salykmaster.shep.wservice.controllers.dto.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Form91000v20Factory {

    public static SonoFno convertToModel(FnoDto fnoDto) {

        ObjectFactory objectFactory = new ObjectFactory();

        SonoFno fno = null;

        if (fnoDto != null) {

            fno = objectFactory.createSonoFno();
            fno.setCode(fnoDto.getCode());
            fno.setFormatVersion(fnoDto.getFormatVersion());
            fno.setVersion(fnoDto.getVersion());

            if (fnoDto.getForms() != null) {
                for (FormDto formDto : fnoDto.getForms()) {
                    SonoForm form = objectFactory.createSonoForm();
                    form.setName(formDto.getName());
                    fno.getForm().add(form);

                    if (formDto.getSheetGroup() != null) {
                        SonoSheetGroup sheetGroup = objectFactory.createSonoSheetGroup();
                        form.setSheetGroup(sheetGroup);

                        if (formDto.getSheetGroup().getSheets() != null) {
                            for (SheetDto sheetDto : formDto.getSheetGroup().getSheets()) {
                                SonoSheet sheet = objectFactory.createSonoSheet();
                                sheet.setName(sheetDto.getName());
                                sheetGroup.getSheet().add(sheet);

                                if (sheetDto.getFields() != null) {
                                    for (FieldDto fieldDto : sheetDto.getFields()) {
                                        SonoField field = objectFactory.createSonoField();
                                        field.setName(fieldDto.getName());
                                        field.setValue(fieldDto.getValue());
                                        sheet.getField().add(field);
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        return fno;
    }

    public static FnoDto convertToDto(SonoFno sonoFno) {

        FnoDto fno = null;

        if (sonoFno != null) {
            fno = new FnoDto();
            fno.setCode(sonoFno.getCode());
            fno.setFormatVersion(sonoFno.getFormatVersion());
            fno.setVersion(sonoFno.getVersion());

            if (sonoFno.getForm() != null) {
                for (SonoForm sonoForm : sonoFno.getForm()) {
                    FormDto form = new FormDto();
                    form.setName(sonoForm.getName());
                    fno.getForms().add(form);

                    if (sonoForm.getSheetGroup() != null) {
                        SheetGroupDto sheetGroup = new SheetGroupDto();
                        form.setSheetGroup(sheetGroup);

                        if (sonoForm.getSheetGroup().getSheet() != null) {
                            for (SonoSheet sonoSheet : sonoForm.getSheetGroup().getSheet()) {
                                SheetDto sheet = new SheetDto();
                                sheet.setName(sonoSheet.getName());
                                sheetGroup.getSheets().add(sheet);

                                if (sonoSheet.getField() != null) {
                                    for (SonoField sonoField : sonoSheet.getField()) {
                                        FieldDto field = new FieldDto();
                                        field.setName(sonoField.getName());
                                        field.setValue(sonoField.getValue());
                                        sheet.getFields().add(field);
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        return fno;
    }

    public static SonoFno createEmptySonoFno() {

        ObjectFactory objectFactory = new ObjectFactory();

        LocalDate today = LocalDate.now();

        String periodHalfYear = today.getMonthValue() <= 6 ? "1" : "2";
        String currencyCode = "KZT";
        String periodYear = today.getYear() + "";
        String formattedToday = today.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        String requiredForFill = "required to fill";
        String iin = "920110300892";
        String dt_main = "false";
        String dt_additional= "false";
        String dt_regular = "true";
        String dt_notice = "false";
        String rating_auth_code = "6207";
        String in_doc_number = "123";
        String payer_name1 = "Есмұхамедов";
        String payer_name2 = "Нұрғали";
        String payer_name3 = "Сейтқалиұлы";
        String head_name = "Есмұхамедов Нұрғали Сейтқалиұлы";
        String receptor_name = "пустая";

        SonoFno fno = objectFactory.createSonoFno();

        fno.setCode("910.00");
        fno.setFormatVersion("1");
        fno.setVersion("20");

        SonoForm form = objectFactory.createSonoForm();
        form.setName("form_910_00");
        fno.getForm().add(form);

        SonoSheetGroup sheetGroup = objectFactory.createSonoSheetGroup();
        form.setSheetGroup(sheetGroup);

        SonoSheet page_910_00_01 = objectFactory.createSonoSheet();
        page_910_00_01.setName("page_910_00_01");
        sheetGroup.getSheet().add(page_910_00_01);

        page_910_00_01.getField().add(createField("field_910_00_007", null));
        page_910_00_01.getField().add(createField("field_910_00_005", null));
        page_910_00_01.getField().add(createField("field_910_00_010_2", null));
        page_910_00_01.getField().add(createField("PaperFormsBarcode1", null));
        page_910_00_01.getField().add(createField("field_910_00_003_a", null));
        page_910_00_01.getField().add(createField("cb_n1", null));
        page_910_00_01.getField().add(createField("cb_n2", null));
        page_910_00_01.getField().add(createField("period_half_year", periodHalfYear));
        page_910_00_01.getField().add(createField("payer_name3", payer_name3));
        page_910_00_01.getField().add(createField("field_910_00_009", null));
        page_910_00_01.getField().add(createField("payer_name1", payer_name1));
        page_910_00_01.getField().add(createField("dt_additional", dt_additional));
        page_910_00_01.getField().add(createField("field_910_00_004", null));
        page_910_00_01.getField().add(createField("field_910_00_010_6", null));
        page_910_00_01.getField().add(createField("rnn", null));
        page_910_00_01.getField().add(createField("field_910_00_010_3", null));
        page_910_00_01.getField().add(createField("payer_name2", payer_name2));
        page_910_00_01.getField().add(createField("field_910_00_010", null));
        page_910_00_01.getField().add(createField("currency_code", currencyCode));
        page_910_00_01.getField().add(createField("period_year", periodYear));
        page_910_00_01.getField().add(createField("field_910_00_010_5", null));
        page_910_00_01.getField().add(createField("field_910_00_003", null));
        page_910_00_01.getField().add(createField("dt_final", null));
        page_910_00_01.getField().add(createField("notice_date", null));
        page_910_00_01.getField().add(createField("field_910_00_008", null));
        page_910_00_01.getField().add(createField("dt_regular", dt_regular));
        page_910_00_01.getField().add(createField("field_910_00_010_1", null));
        page_910_00_01.getField().add(createField("field_910_00_001", null));
        page_910_00_01.getField().add(createField("dt_main", dt_main));
        page_910_00_01.getField().add(createField("dt_notice", dt_notice));
        page_910_00_01.getField().add(createField("ne_ved_buch", null));
        page_910_00_01.getField().add(createField("field_910_00_010_4", null));
        page_910_00_01.getField().add(createField("iin", iin));

        SonoSheet page_910_00_02 = objectFactory.createSonoSheet();
        page_910_00_02.setName("page_910_00_02");
        sheetGroup.getSheet().add(page_910_00_02);

        page_910_00_02.getField().add(createField("field_910_00_017_1", null));
        page_910_00_02.getField().add(createField("field_910_00_016_5", null));
        page_910_00_02.getField().add(createField("field_910_00_015_1", null));
        page_910_00_02.getField().add(createField("period_year", periodYear));
        page_910_00_02.getField().add(createField("field_910_00_011_3", null));
        page_910_00_02.getField().add(createField("field_910_00_012_4", null));
        page_910_00_02.getField().add(createField("field_910_00_016_1", null));
        page_910_00_02.getField().add(createField("field_910_00_012_6", null));
        page_910_00_02.getField().add(createField("field_910_00_014_6", null));
        page_910_00_02.getField().add(createField("field_910_00_015_4", null));
        page_910_00_02.getField().add(createField("field_910_00_013_4", null));
        page_910_00_02.getField().add(createField("field_910_00_015_6", null));
        page_910_00_02.getField().add(createField("field_910_00_017_6", null));
        page_910_00_02.getField().add(createField("field_910_00_011_5", null));
        page_910_00_02.getField().add(createField("field_910_00_011", null));
        page_910_00_02.getField().add(createField("PaperFormsBarcode2", null));
        page_910_00_02.getField().add(createField("field_910_00_014_4", null));
        page_910_00_02.getField().add(createField("field_910_00_016_2", null));
        page_910_00_02.getField().add(createField("field_910_00_014_2", null));
        page_910_00_02.getField().add(createField("field_910_00_014_1", null));
        page_910_00_02.getField().add(createField("field_910_00_012_5", null));
        page_910_00_02.getField().add(createField("field_910_00_013", null));
        page_910_00_02.getField().add(createField("field_910_00_014_3", null));
        page_910_00_02.getField().add(createField("field_910_00_013_5", null));
        page_910_00_02.getField().add(createField("field_910_00_012_1", null));
        page_910_00_02.getField().add(createField("field_910_00_017_5", null));
        page_910_00_02.getField().add(createField("period_half_year", periodHalfYear));
        page_910_00_02.getField().add(createField("field_910_00_016_3", null));
        page_910_00_02.getField().add(createField("field_910_00_011_4", null));
        page_910_00_02.getField().add(createField("field_910_00_015_5", null));
        page_910_00_02.getField().add(createField("field_910_00_013_6", null));
        page_910_00_02.getField().add(createField("field_910_00_017", null));
        page_910_00_02.getField().add(createField("field_910_00_014", null));
        page_910_00_02.getField().add(createField("field_910_00_017_3", null));
        page_910_00_02.getField().add(createField("field_910_00_016_4", null));
        page_910_00_02.getField().add(createField("field_910_00_015", null));
        page_910_00_02.getField().add(createField("field_910_00_013_3", null));
        page_910_00_02.getField().add(createField("field_910_00_014_5", null));
        page_910_00_02.getField().add(createField("field_910_00_013_2", null));
        page_910_00_02.getField().add(createField("field_910_00_015_2", null));
        page_910_00_02.getField().add(createField("field_910_00_011_1", null));
        page_910_00_02.getField().add(createField("field_910_00_015_3", null));
        page_910_00_02.getField().add(createField("field_910_00_013_1", null));
        page_910_00_02.getField().add(createField("iin", iin));
        page_910_00_02.getField().add(createField("field_910_00_011_2", null));
        page_910_00_02.getField().add(createField("field_910_00_017_4", null));
        page_910_00_02.getField().add(createField("field_910_00_012", null));
        page_910_00_02.getField().add(createField("field_910_00_016", null));
        page_910_00_02.getField().add(createField("field_910_00_011_6", null));
        page_910_00_02.getField().add(createField("field_910_00_012_3", null));
        page_910_00_02.getField().add(createField("field_910_00_017_2", null));
        page_910_00_02.getField().add(createField("field_910_00_012_2", null));

        SonoSheet page_910_00_03 = objectFactory.createSonoSheet();
        page_910_00_03.setName("page_910_00_03");
        sheetGroup.getSheet().add(page_910_00_03);

        page_910_00_03.getField().add(createField("field_910_00_021_2", null));
        page_910_00_03.getField().add(createField("field_910_00_020_5", null));
        page_910_00_03.getField().add(createField("field_910_00_021_4", null));
        page_910_00_03.getField().add(createField("period_half_year", periodHalfYear));
        page_910_00_03.getField().add(createField("field_910_00_023_5", null));
        page_910_00_03.getField().add(createField("field_910_00_020", null));
        page_910_00_03.getField().add(createField("field_910_00_019_4", null));
        page_910_00_03.getField().add(createField("field_910_00_018_3", null));
        page_910_00_03.getField().add(createField("field_910_00_018", null));
        page_910_00_03.getField().add(createField("field_910_00_019_3", null));
        page_910_00_03.getField().add(createField("field_910_00_021", null));
        page_910_00_03.getField().add(createField("field_910_00_023", null));
        page_910_00_03.getField().add(createField("field_910_00_022_5", null));
        page_910_00_03.getField().add(createField("field_910_00_019", null));
        page_910_00_03.getField().add(createField("field_910_00_022_2", null));
        page_910_00_03.getField().add(createField("field_910_00_020_3", null));
        page_910_00_03.getField().add(createField("field_910_00_024_4", null));
        page_910_00_03.getField().add(createField("field_910_00_023_2", null));
        page_910_00_03.getField().add(createField("field_910_00_022", null));
        page_910_00_03.getField().add(createField("field_910_00_022_6", null));
        page_910_00_03.getField().add(createField("field_910_00_024_3", null));
        page_910_00_03.getField().add(createField("field_910_00_018_6", null));
        page_910_00_03.getField().add(createField("field_910_00_022_3", null));
        page_910_00_03.getField().add(createField("field_910_00_023_1", null));
        page_910_00_03.getField().add(createField("field_910_00_022_4", null));
        page_910_00_03.getField().add(createField("iin", iin));
        page_910_00_03.getField().add(createField("field_910_00_021_5", null));
        page_910_00_03.getField().add(createField("field_910_00_019_2", null));
        page_910_00_03.getField().add(createField("field_910_00_022_1", null));
        page_910_00_03.getField().add(createField("field_910_00_019_5", null));
        page_910_00_03.getField().add(createField("field_910_00_018_2", null));
        page_910_00_03.getField().add(createField("field_910_00_021_6", null));
        page_910_00_03.getField().add(createField("field_910_00_023_4", null));
        page_910_00_03.getField().add(createField("field_910_00_021_1", null));
        page_910_00_03.getField().add(createField("field_910_00_018_5", null));
        page_910_00_03.getField().add(createField("field_910_00_023_3", null));
        page_910_00_03.getField().add(createField("field_910_00_024", null));
        page_910_00_03.getField().add(createField("field_910_00_020_1", null));
        page_910_00_03.getField().add(createField("field_910_00_019_1", null));
        page_910_00_03.getField().add(createField("field_910_00_020_4", null));
        page_910_00_03.getField().add(createField("field_910_00_021_3", null));
        page_910_00_03.getField().add(createField("period_year", periodYear));
        page_910_00_03.getField().add(createField("field_910_00_024_1", null));
        page_910_00_03.getField().add(createField("field_910_00_019_6", null));
        page_910_00_03.getField().add(createField("field_910_00_020_6", null));
        page_910_00_03.getField().add(createField("field_910_00_018_4", null));
        page_910_00_03.getField().add(createField("field_910_00_020_2", null));
        page_910_00_03.getField().add(createField("field_910_00_024_6", null));
        page_910_00_03.getField().add(createField("field_910_00_018_1", null));
        page_910_00_03.getField().add(createField("field_910_00_024_2", null));
        page_910_00_03.getField().add(createField("field_910_00_024_5", null));
        page_910_00_03.getField().add(createField("PaperFormsBarcode3", null));

        SonoSheet page_910_00_04 = objectFactory.createSonoSheet();
        page_910_00_04.setName("page_910_00_04");
        sheetGroup.getSheet().add(page_910_00_04);

        page_910_00_04.getField().add(createField("rating_auth_code_struct", null));
        page_910_00_04.getField().add(createField("period_year", periodYear));
        page_910_00_04.getField().add(createField("in_doc_number", in_doc_number));
        page_910_00_04.getField().add(createField("PaperFormsBarcode4", null));
        page_910_00_04.getField().add(createField("head_name", head_name));
        page_910_00_04.getField().add(createField("submit_date", formattedToday));
        page_910_00_04.getField().add(createField("post_date", formattedToday));
        page_910_00_04.getField().add(createField("field_910_00_025", null));
        page_910_00_04.getField().add(createField("receptor_name", receptor_name));
        page_910_00_04.getField().add(createField("rating_auth_code", rating_auth_code));
        page_910_00_04.getField().add(createField("iin", iin));
        page_910_00_04.getField().add(createField("accept_date", formattedToday));
        page_910_00_04.getField().add(createField("period_half_year", periodHalfYear));


        return fno;
    }

    public static SonoField createField(String name, String value) {
        SonoField field = new SonoField();
        field.setName(name);
        field.setValue(value);
        return field;
    }

}
