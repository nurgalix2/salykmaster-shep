package kz.salykmaster.shep.wservice.handlers;

import kz.gov.pki.kalkan.asn1.pkcs.PKCSObjectIdentifiers;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import kz.salykmaster.shep.wservice.utils.x509utils.FileSystemFunctions;
import lombok.SneakyThrows;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.token.SecurityTokenReference;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.encryption.XMLCipherParameters;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


public class MessageHandler implements SOAPHandler<SOAPMessageContext> {
    //    private static final String SHEP_CERT_FILE = "certs/shep_cert.cer";
    private static final String SHEP_CERT_FILE = "keys/pub_cert2.cer";
//    private static final String KEY_PATH = "C:\\work\\keys\\keys\\GOSTKNCA_35ba826a3c4cbca1317a9a6ee06fc18a2684d8e3.p12";
//    private static final String KEY_PASS = "A123456a";

    private static final String KEY_PATH = "C:\\work\\keys\\keys\\GOSTKNCA_20e3ee4934461a3e69da1352c4c08999c654fa7f.p12";
    private static final String KEY_PASS = "Aa1234";

//    InputStream inStream = getClass().getClassLoader().getResourceAsStream(SHEP_CERT_FILE);
//    final PrivateKey signPrivateKey = FileSystemFunctions.loadKeyFromFile(KEY_PATH, "PKCS12", KEY_PASS);
//    final X509Certificate signX509Certificate = FileSystemFunctions.loadCertFromFile(KEY_PATH, "PKCS12", KEY_PASS);

    public static final String WSU_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
    public static final String WSU_PREFIX = "wsu";

    @Override
    public Set<QName> getHeaders() {
        QName qName = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security");
        HashSet<QName> hashSet = new HashSet<>();
        hashSet.add(qName);
        return hashSet;
    }

    @SneakyThrows
    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        System.out.println("-------------handleMessage-------------");
        Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        SOAPMessageContext soapMessageContext = (SOAPMessageContext) context;
        SOAPMessage soapMessage = soapMessageContext.getMessage();
        soapMessage.setProperty(javax.xml.soap.SOAPMessage.CHARACTER_SET_ENCODING, "UTF-8");

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        try {
            soapMessage.writeTo(arrayOutputStream);
            String xml = new String(arrayOutputStream.toByteArray(), StandardCharsets.UTF_8);

            xml = xml.replace("&#13;", "");

            if (outboundProperty.booleanValue()) {
                System.out.println("Sign soap");
                SOAPMessage signedSoap = sign(xml);
                context.getMessage().getSOAPPart().setContent(signedSoap.getSOAPPart().getContent());

                ByteArrayOutputStream arrayOutputStream1 = new ByteArrayOutputStream();
                signedSoap.writeTo(arrayOutputStream1);
                System.out.println(new String(arrayOutputStream1.toByteArray(), StandardCharsets.UTF_8));
                return true;
            } else {
                if (!xml.contains("wsse:Security")) {
                    System.out.println("Отсутствует транспортная ЭЦП");
                    SOAPMessage message = getErrorSoap(xml, "SIGN_NOT_EXIST");
                    soapMessage.getSOAPPart().setContent(message.getSOAPPart().getContent());
                }
//                else {
//                    System.out.println("Имеется транспортная ЭЦП");
//                    if (verifyXml(xml, getCertificate())) {
//                        SOAPMessage message = getSoapWithoutSign(xml);
//                        soapMessage.getSOAPPart().setContent(message.getSOAPPart().getContent());
//                    } else {
//                        System.out.println("ЭЦП отрицательная");
//                        SOAPMessage message = getErrorSoap(xml, "IS_NOT_VALID");
//                        soapMessage.getSOAPPart().setContent(message.getSOAPPart().getContent());
//                    }
//                }
            }
        } catch (SOAPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public void close(MessageContext context) {
        System.out.println("On Close handler");
    }

    public boolean verifyXml(String xmlString, X509Certificate x509Certificate) {
        KalkanProvider kalkanProvider = new KalkanProvider();
        Security.addProvider(kalkanProvider);
        KncaXS.loadXMLSecurity();

        System.out.println("test->\n" + xmlString);

        InputStream inStream = null;
        X509Certificate cert = null;
        boolean result = false;
        try {
            inStream = getClass().getClassLoader().getResourceAsStream(SHEP_CERT_FILE);

            CertificateFactory cf = CertificateFactory.getInstance("X.509", kalkanProvider.getName());
            cert = (X509Certificate) cf.generateCertificate(inStream);

//            final PrivateKey privateKey = FileSystemFunctions.loadKeyFromFile(KEY_PATH,"PKCS12", KEY_PASS);
//            cert = FileSystemFunctions.loadCertFromFile(KEY_PATH,"PKCS12", KEY_PASS);

            System.out.println("dn: " + cert.getIssuerDN().toString());
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document doc = documentBuilder.parse(new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8)));

            Element sigElement = null;
            Element rootEl = (Element) doc.getFirstChild();
            NodeList list = rootEl.getElementsByTagName("ds:Signature");
            int length = list.getLength();
            System.out.println(length);
            for (int i = 0; i < length; i++) {
                Node sigNode = list.item(length - 1);
                sigElement = (Element) sigNode;
                if (sigElement == null) {
                    System.err.println("Bad signature: Element 'ds:Reference' is not found in XML document");
                }
                XMLSignature signature = new XMLSignature(sigElement, "");
                result = signature.checkSignatureValue(cert);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("VERIFICATION RESULT IS: " + result);

        return result;
    }

    public SOAPMessage sign(final String SIMPLE_XML_SOAP) throws SOAPException, IOException, XMLSecurityException, NoSuchProviderException, KeyStoreException, PrivilegedActionException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        KalkanProvider kalkanProvider = new KalkanProvider();
        Security.addProvider(kalkanProvider);
        KncaXS.loadXMLSecurity();

        final String signMethod;
        final String digestMethod;
        InputStream is = new ByteArrayInputStream(SIMPLE_XML_SOAP.getBytes(StandardCharsets.UTF_8));
        try {
            SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
            msg.setProperty(javax.xml.soap.SOAPMessage.CHARACTER_SET_ENCODING, "UTF-8");

            SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
            SOAPBody body = env.getBody();

            String bodyId = "id-" + UUID.randomUUID().toString();
            body.addAttribute(new QName(WSU_NS, "Id", WSU_PREFIX), bodyId);

            SOAPHeader header = env.getHeader();
            if (header == null) {
                header = env.addHeader();
            }

//            InputStream inStream = getClass().getClassLoader().getResourceAsStream(SHEP_CERT_FILE);
//            final PrivateKey privateKey = FileSystemFunctions.loadKeyFromFile(KEY_PATH,"PKCS12", KEY_PASS);
//            final X509Certificate x509Certificate = FileSystemFunctions.loadCertFromFile(KEY_PATH,"PKCS12", KEY_PASS);

            KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
            InputStream inputStream;
            inputStream = AccessController.doPrivileged(new PrivilegedExceptionAction<InputStream>() {
                @Override
                public FileInputStream run() throws Exception {
                    return new FileInputStream(KEY_PATH);
                }
            });

            store.load(inputStream, KEY_PASS.toCharArray());
            Enumeration<String> als = store.aliases();
            String alias = null;
            while (als.hasMoreElements()) {
                alias = als.nextElement();
            }
            final PrivateKey privateKey = (PrivateKey) store.getKey(alias, KEY_PASS.toCharArray());
            final X509Certificate x509Certificate = (X509Certificate) store.getCertificate(alias);

            String sigAlgOid = x509Certificate.getSigAlgOID();
            if (sigAlgOid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha1";
                digestMethod = Constants.MoreAlgorithmsSpecNS + "sha1";
            } else if (sigAlgOid.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha256";
                digestMethod = XMLCipherParameters.SHA256;
            } else {
                signMethod = Constants.MoreAlgorithmsSpecNS + "gost34310-gost34311";
                digestMethod = Constants.MoreAlgorithmsSpecNS + "gost34311";
            }

            Document doc = env.getOwnerDocument();
            Transforms transforms = new Transforms(env.getOwnerDocument());
            transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
            Element c14nMethod = XMLUtils.createElementInSignatureSpace(doc, "CanonicalizationMethod");
            c14nMethod.setAttributeNS(null, "Algorithm", Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);

            Element signatureMethod = XMLUtils.createElementInSignatureSpace(doc, "SignatureMethod");
            signatureMethod.setAttributeNS(null, "Algorithm", signMethod);

            XMLSignature sig = new XMLSignature(env.getOwnerDocument(), "", signatureMethod, c14nMethod);

            sig.addDocument("#" + bodyId, transforms, digestMethod);
            sig.getSignedInfo().getSignatureMethodElement().setNodeValue(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);

            WSSecHeader secHeader = new WSSecHeader();
            secHeader.setMustUnderstand(true);
            secHeader.insertSecurityHeader(env.getOwnerDocument());
            secHeader.getSecurityHeader().appendChild(sig.getElement());
            header.appendChild(secHeader.getSecurityHeader());

            SecurityTokenReference reference = new SecurityTokenReference(doc);
            reference.setKeyIdentifier(x509Certificate);

            sig.getKeyInfo().addUnknownElement(reference.getElement());
            sig.sign(privateKey);

            String signedSoap = PrettyDocumentToString(doc);

            SOAPMessage soapMessage = createSOAPFromString(signedSoap);
            return soapMessage;

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static String PrettyDocumentToString(Document doc) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ElementToStream(doc.getDocumentElement(), baos);
        return new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }

    public static void ElementToStream(Element element, OutputStream out) {
        try {
            DOMSource source = new DOMSource(element);
            StreamResult result = new StreamResult(out);
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(source, result);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    private X509Certificate getCertificate() {
        KalkanProvider kalkanProvider = new KalkanProvider();
        Security.addProvider(kalkanProvider);
        KncaXS.loadXMLSecurity();
        InputStream inStream = null;
        try {
            inStream = getClass().getClassLoader().getResourceAsStream(SHEP_CERT_FILE);

            CertificateFactory cf = CertificateFactory.getInstance("X.509", kalkanProvider.getName());
            X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
            return cert;

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private SOAPMessage getSoapWithoutSign(String xml) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = dbf.newDocumentBuilder();

            Document doc = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

            Element rootEl = (Element) doc.getFirstChild();
            NodeList list1 = rootEl.getElementsByTagName("wsse:Security");
            if (list1 != null) {
                if (list1.getLength() > 0) {
                    Node wsseNode = list1.item(0);
                    wsseNode.getParentNode().removeChild(wsseNode);
                }
            }
            String diffXml = nodeToString(rootEl);
            System.out.println("XML without sign");
            System.out.println(diffXml);
            SOAPMessage message = createSOAPFromString(diffXml);
            try {
                message.getSOAPBody().removeAttributeNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Id");
                message.getSOAPBody().removeNamespaceDeclaration("wsu");
            } catch (SOAPException e) {
                e.printStackTrace();
            }
            return message;
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    private SOAPMessage getErrorSoap(String xml, String errorText) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = dbf.newDocumentBuilder();

            Document doc = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

            Element rootEl = (Element) doc.getFirstChild();
            NodeList list1 = rootEl.getElementsByTagName("wsse:Security");
            if (list1 != null) {
                if (list1.getLength() > 0) {
                    Node wsseNode = list1.item(0);
                    wsseNode.getParentNode().removeChild(wsseNode);
                }
            }

            Node node = rootEl.getElementsByTagName("sessionId").item(0);
            node.setTextContent(errorText);

            String diffXml = nodeToString(rootEl);
            SOAPMessage message = createSOAPFromString(diffXml);
            try {
                message.getSOAPBody().removeAttributeNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Id");
                message.getSOAPBody().removeNamespaceDeclaration("wsu");
            } catch (SOAPException e) {
                e.printStackTrace();
            }
            return message;
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }

    public static SOAPMessage createSOAPFromString(String xmlString) {
        SOAPMessage message = null;
        try {
            message = MessageFactory.newInstance().createMessage();
            message.setProperty(javax.xml.soap.SOAPMessage.CHARACTER_SET_ENCODING, "UTF-8");
            SOAPPart soapPart = message.getSOAPPart();
            ByteArrayInputStream stream = null;
            stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
            StreamSource source = new StreamSource(stream);
            soapPart.setContent(source);
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return message;
    }
}