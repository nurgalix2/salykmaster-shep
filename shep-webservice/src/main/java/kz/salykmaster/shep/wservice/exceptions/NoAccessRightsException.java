package kz.salykmaster.shep.wservice.exceptions;

public class NoAccessRightsException extends Exception {

    public NoAccessRightsException() {
        super();
    }

    public NoAccessRightsException(String message) {
        super(message);
    }

    public NoAccessRightsException(Long id) {
        super("Not found for this id: " + id);
    }

}