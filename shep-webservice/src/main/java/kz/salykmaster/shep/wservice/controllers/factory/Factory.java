package kz.salykmaster.shep.wservice.controllers.factory;

import kz.salykmaster.shep.service.sync.SyncSendMessageResponse;
import kz.salykmaster.shep.sono.types.ObjectFactory;
import kz.salykmaster.shep.sono.types.StatusRequest;
import kz.salykmaster.shep.wservice.controllers.dto.soap.MessageInfoResponse;
import kz.salykmaster.shep.wservice.controllers.dto.soap.ResponseData;
import kz.salykmaster.shep.wservice.controllers.dto.soap.SendMessageResponse;
import kz.salykmaster.shep.wservice.controllers.dto.soap.StatusInfo;

public class Factory {
    public static SendMessageResponse convertToDto(SyncSendMessageResponse syncSendMessageResponse) {

        SendMessageResponse sendMessageResponse = null;

        if (syncSendMessageResponse != null) {

            sendMessageResponse = new SendMessageResponse();

            if (syncSendMessageResponse.getResponseData() != null) {

                ResponseData responseData = new ResponseData();
                responseData.setData(syncSendMessageResponse.getResponseData().getData());

                sendMessageResponse.setResponseData(responseData);
            }

            if (syncSendMessageResponse.getResponseInfo() != null) {
                MessageInfoResponse messageInfoResponse = new MessageInfoResponse();
                messageInfoResponse.setMessageId(syncSendMessageResponse.getResponseInfo().getMessageId());
                messageInfoResponse.setCorrelationId(syncSendMessageResponse.getResponseInfo().getCorrelationId());
                if (syncSendMessageResponse.getResponseInfo().getResponseDate() != null)
                    messageInfoResponse.setResponseDate(syncSendMessageResponse.getResponseInfo().getResponseDate().toGregorianCalendar().getTime());
                messageInfoResponse.setSessionId(syncSendMessageResponse.getResponseInfo().getSessionId());

                if (syncSendMessageResponse.getResponseInfo().getStatus() != null) {
                    StatusInfo statusInfo = new StatusInfo();
                    statusInfo.setCode(syncSendMessageResponse.getResponseInfo().getStatus().getCode());
                    statusInfo.setMessage(syncSendMessageResponse.getResponseInfo().getStatus().getMessage());
                    messageInfoResponse.setStatus(statusInfo);
                }
                sendMessageResponse.setResponseInfo(messageInfoResponse);
            }

        }

        return sendMessageResponse;

    }

    public static StatusRequest createStatusRequest(Long id){

        ObjectFactory objectFactory = new ObjectFactory();

        StatusRequest statusRequest = objectFactory.createStatusRequest();
        statusRequest.setId(id);

        return statusRequest;
    }
}
