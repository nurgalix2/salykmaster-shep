package kz.salykmaster.shep.wservice.exceptions;

public class ResourceInformationException extends Exception {

    public ResourceInformationException() {
        super();
    }

    public ResourceInformationException(String message) {
        super(message);
    }

}