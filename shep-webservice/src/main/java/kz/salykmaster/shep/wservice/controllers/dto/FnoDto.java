package kz.salykmaster.shep.wservice.controllers.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class FnoDto {

    @Getter
    @Setter
    String code;

    @Getter
    @Setter
    String formatVersion;

    @Getter
    @Setter
    String version;

    List<FormDto> forms;

    public List<FormDto> getForms() {
        if (this.forms == null) {
            this.forms = new ArrayList<>();
        }

        return this.forms;
    }

}
