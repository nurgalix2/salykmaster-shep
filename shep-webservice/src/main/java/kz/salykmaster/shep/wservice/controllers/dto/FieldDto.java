package kz.salykmaster.shep.wservice.controllers.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class FieldDto {
    String name;
    String value;
}
