package kz.salykmaster.shep.wservice.controllers.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class SheetGroupDto {
    List<SheetDto> sheets;
    public List<SheetDto> getSheets() {
        if (this.sheets == null) {
            this.sheets = new ArrayList<>();
        }

        return this.sheets;
    }
}
