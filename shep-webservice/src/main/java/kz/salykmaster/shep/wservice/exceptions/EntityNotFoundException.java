package kz.salykmaster.shep.wservice.exceptions;

public class EntityNotFoundException extends Exception {

    public EntityNotFoundException() {
        super();
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(Long id) {
        super("Not found for this id: " + id);
    }

}
