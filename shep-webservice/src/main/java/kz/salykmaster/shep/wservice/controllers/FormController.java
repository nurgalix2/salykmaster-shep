package kz.salykmaster.shep.wservice.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import kz.salykmaster.shep.sono.form91000v20.ObjectFactory;
import kz.salykmaster.shep.sono.form91000v20.SonoFno;
import kz.salykmaster.shep.sono.types.StatusRequest;
import kz.salykmaster.shep.wservice.controllers.factory.Factory;
import kz.salykmaster.shep.wservice.controllers.factory.Form91000v20Factory;
import kz.salykmaster.shep.wservice.payload.response.ResponseHelper;
import kz.salykmaster.shep.wservice.service.FilesStorageService;
import kz.salykmaster.shep.wservice.utils.x509utils.FileSystemFunctions;
import kz.salykmaster.shep.wservice.utils.x509utils.VerificationData;
import kz.salykmaster.shep.wservice.utils.x509utils.VerificationResult;
import kz.salykmaster.shep.wservice.utils.xmlds.XmlDsUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/form")
@Api(tags = {"Вспомогательный API для форм"})
public class FormController {

    static {
        KalkanProvider kalkanProvider = new KalkanProvider();
        Security.addProvider(kalkanProvider);
        KncaXS.loadXMLSecurity();
    }

    Logger logger = LoggerFactory.getLogger(FormController.class);

    @Autowired
    FilesStorageService storageService;

    @ApiOperation(value = "Получить пустую форму 910.00v20", response = ResponseHelper.class)
    @GetMapping(value = "/blank_form_91000v20")
    public ResponseHelper getBlankForm91000v20() {
        try {

            ObjectFactory objectFactory = new ObjectFactory();

            SonoFno sonoFno = Form91000v20Factory.createEmptySonoFno();

            JAXBContext jaxbContext = JAXBContext.newInstance(SonoFno.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            StringWriter writer = new StringWriter();
            marshaller.marshal(objectFactory.createFno(sonoFno), writer);

            String result = writer.toString();

            return new ResponseHelper(result, HttpStatus.OK);

        } catch (Exception error) {
            error.printStackTrace();
            return new ResponseHelper(error);
        }

    }


    @ApiOperation(value = "Подписать форму", response = ResponseHelper.class)
    @PostMapping(value = "/sign_form", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseHelper signForm(@ApiParam(name = "RSACertificate", value = "RSA сертификат владельца ИП")
                                   @RequestPart("RSACertificate") MultipartFile RSACertificate,
                                   @ApiParam(name = "privateKeyPassword", value = "Пароль закрытого ключа RSA сертификата")
                                   @RequestPart("privateKeyPassword") String privateKeyPassword,
                                   @ApiParam(name = "xml", value = "Форма виде XML")
                                   @RequestPart String xml) {
        try {

            xml = xml.replace("\\r\\n", System.lineSeparator()).replace("\\", "");

            String keyPath = storageService.save(RSACertificate);

            final PrivateKey privateKey = FileSystemFunctions.loadKeyFromFile2(keyPath, "PKCS12", privateKeyPassword);
            final X509Certificate x509Certificate = FileSystemFunctions.loadCertFromFile2(keyPath, "PKCS12", privateKeyPassword);

            String result = XmlDsUtils.signXML(xml, x509Certificate, privateKey);

            return new ResponseHelper(result, HttpStatus.OK);

        } catch (Exception error) {
            error.printStackTrace();
            return new ResponseHelper(error);
        }

    }

    @ApiOperation(value = "Подписать запрос для получения статуса", response = ResponseHelper.class)
    @PostMapping(value = "/sign_status_request", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseHelper signRequestStatus(@ApiParam(name = "RSACertificate", value = "RSA сертификат владельца ИП")
                                            @RequestPart("RSACertificate") MultipartFile RSACertificate,
                                            @ApiParam(name = "privateKeyPassword", value = "Пароль закрытого ключа RSA сертификата")
                                            @RequestPart("privateKeyPassword") String privateKeyPassword,
                                            @ApiParam(name = "id", value = "ИД запроса")
                                            @RequestPart String id) {
        try {

            kz.salykmaster.shep.sono.types.ObjectFactory objectFactory = new kz.salykmaster.shep.sono.types.ObjectFactory();

            StatusRequest statusRequest = Factory.createStatusRequest(Long.parseLong(id));

            JAXBContext jaxbContext = JAXBContext.newInstance(StatusRequest.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            StringWriter writer = new StringWriter();
            marshaller.marshal(objectFactory.createStatusRequest(statusRequest), writer);

            String xml = writer.toString();

            String keyPath = storageService.save(RSACertificate);

            final PrivateKey privateKey = FileSystemFunctions.loadKeyFromFile2(keyPath, "PKCS12", privateKeyPassword);
            final X509Certificate x509Certificate = FileSystemFunctions.loadCertFromFile2(keyPath, "PKCS12", privateKeyPassword);

            String result = XmlDsUtils.signXML(xml, x509Certificate, privateKey);

            return new ResponseHelper(result, HttpStatus.OK);

        } catch (Exception error) {
            error.printStackTrace();
            return new ResponseHelper(error);
        }

    }

    @ApiOperation(value = "Проверить подпись", response = ResponseHelper.class)
    @PostMapping(value = "/verify_sign", consumes = {MediaType.TEXT_PLAIN_VALUE})
    public ResponseHelper verifySignedForm(@ApiParam(name = "signedXML", value = "Подписанная XML")
                                           @RequestBody String signedXML) {
        try {

            signedXML = signedXML.replace("\\r\\n", System.lineSeparator()).replace("\\", "");

            List<VerificationResponse> response = new ArrayList<>();

            VerificationData[] verificationData = XmlDsUtils.validateXMLSignatures(signedXML);

            for (VerificationData vData : verificationData) {
                response.add(new VerificationResponse(vData.getVerificationResult(), vData.getSubjectDN()));
            }

            return new ResponseHelper(response);

        } catch (Exception error) {
            error.printStackTrace();
            return new ResponseHelper(error);
        }

    }

    @Data
    @AllArgsConstructor
    static class VerificationResponse {
        private VerificationResult verificationResult;
        private String subjectDN;
    }

}
