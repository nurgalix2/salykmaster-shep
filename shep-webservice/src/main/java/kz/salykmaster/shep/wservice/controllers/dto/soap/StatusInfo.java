package kz.salykmaster.shep.wservice.controllers.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatusInfo {
    private String code;
    private String message;
}
