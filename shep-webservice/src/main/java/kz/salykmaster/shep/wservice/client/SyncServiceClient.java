package kz.salykmaster.shep.wservice.client;

import kz.salykmaster.shep.wservice.handlers.MessageHandler;
import kz.salykmaster.shep.service.sync.*;
import kz.salykmaster.shep.wservice.utils.xmlds.XmlDsUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.CDATASection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class SyncServiceClient {
    private static final String SERVICE_ENDPOINT = "https://egov.kz:19023/bip-sync-wss-gost/";;
    private ISyncChannel syncChannel;

    public SyncServiceClient() {
        ISyncChannelHttpService syncChannelService = new ISyncChannelHttpService();
        syncChannel = syncChannelService.getSyncChannelHttpPort();
        BindingProvider bp = (BindingProvider) syncChannel;
        Handler handler = new MessageHandler();
        List<Handler> handlers = new ArrayList<>();
        handlers.add(handler);
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, SERVICE_ENDPOINT);
        bp.getBinding().setHandlerChain(handlers);
    }

    public SyncSendMessageResponse sendMessage(String serviceId, String xml) throws ParserConfigurationException, SendMessageSendMessageFaultMsg {
        SyncSendMessageRequest request = new SyncSendMessageRequest();

        SyncMessageInfo syncMessageInfo = new SyncMessageInfo();
        syncMessageInfo.setMessageDate(XmlDsUtils.dateToCalendar(new Date()));
        syncMessageInfo.setServiceId(serviceId);
        syncMessageInfo.setMessageId(UUID.randomUUID().toString());
        SenderInfo senderInfo = new SenderInfo();
        senderInfo.setSenderId("Salykmaster2020");
        senderInfo.setPassword("(A697fon)");
        syncMessageInfo.setSender(senderInfo);
        RequestData data = new RequestData();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder documentBuilder = dbf.newDocumentBuilder();

        CDATASection cdata = documentBuilder.newDocument().createCDATASection(xml);
        System.out.println(cdata.getTextContent());
        data.setData(cdata.getTextContent());

        request.setRequestInfo(syncMessageInfo);
        request.setRequestData(data);

        return syncChannel.sendMessage(request);
    }


}
