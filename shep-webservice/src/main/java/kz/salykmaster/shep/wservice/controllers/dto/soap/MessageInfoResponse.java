package kz.salykmaster.shep.wservice.controllers.dto.soap;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageInfoResponse {
    private String messageId;
    private String correlationId;
    @JsonFormat(
            shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ", locale = "kk_KZ")
    private Date responseDate;
    private StatusInfo status;
    private String sessionId;
}
