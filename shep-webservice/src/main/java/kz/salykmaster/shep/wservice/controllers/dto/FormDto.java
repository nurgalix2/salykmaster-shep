package kz.salykmaster.shep.wservice.controllers.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FormDto {
    String name;
    SheetGroupDto sheetGroup;
}
