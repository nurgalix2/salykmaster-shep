package kz.salykmaster.shep.wservice.controllers.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class SheetDto {

    @Getter
    @Setter
    String name;

    List<FieldDto> fields;

    public List<FieldDto> getFields() {
        if (this.fields == null) {
            this.fields = new ArrayList<>();
        }

        return this.fields;
    }
}
