package kz.salykmaster.shep.wservice.payload.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import kz.salykmaster.shep.service.sync.ErrorInfo;
import kz.salykmaster.shep.service.sync.SendMessageSendMessageFaultMsg;
import kz.salykmaster.shep.wservice.exceptions.NoAccessRightsException;
import kz.salykmaster.shep.wservice.exceptions.PaginationSortingException;
import kz.salykmaster.shep.wservice.exceptions.ResourceInformationException;
import kz.salykmaster.shep.wservice.exceptions.ResourceNotFoundException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseHelper implements Serializable {

    /**
     * |-----------------------------------------------------------------------------------------------------|
     * |Type	    |           Description                      |   Required Keys	   |    Optional Keys    |
     * |-----------------------------------------------------------------------------------------------------|
     * |success     |All went well, and (usually) some           |status, data         |                     |
     * |            |data was returned.                          |	                   |                     |
     * |-----------------------------------------------------------------------------------------------------|
     * |fail	    |There was a problem with the data submitted,|status, data         |                     |
     * |            |or some pre-condition of the API call wasn't|                     |                     |
     * |            |satisfied	                                 |                     |                     |
     * |-----------------------------------------------------------------------------------------------------|
     * |error	    |An error occurred in processing the request,|status, message      |code, data           |
     * |            |i.e. an exception was thrown                |		               |                     |
     * |-----------------------------------------------------------------------------------------------------|
     **/

    private HttpStatus status;
    private Object data;
    private String message;
    private Object code;
    private Object trace;

    public ResponseHelper(Object data) {
        this.status = HttpStatus.OK;
        this.data = data;
    }

    public ResponseHelper(List data) {
        this.status = HttpStatus.OK;
        this.data = data;
    }

    public ResponseHelper(String message) {
        this.status = HttpStatus.OK;
        this.message = message;
    }

    public ResponseHelper(Long id) {
        this.status = HttpStatus.OK;
        this.data = id;
    }

    public ResponseHelper(Object data, int status) {
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.data = data;
    }

    public ResponseHelper(Object data, HttpStatus status) {
        this.status = status;
        this.data = data;
    }

    public ResponseHelper(Exception e) {
        if (e instanceof SendMessageSendMessageFaultMsg) {
            SendMessageSendMessageFaultMsg error = (SendMessageSendMessageFaultMsg)e;
            ErrorInfo errorInfo = error.getFaultInfo();
            this.message = error.getMessage();
            this.code = errorInfo.getErrorCode();
            this.data = errorInfo.getErrorData();
            this.status = HttpStatus.INTERNAL_SERVER_ERROR;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else if (e instanceof ResourceNotFoundException) {
            this.message = (e.getMessage() != null) ? e.getMessage() : "not found";
            this.code = HttpStatus.NOT_FOUND.value();
            this.status = HttpStatus.NOT_FOUND;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else if (e instanceof ResourceInformationException) {
            this.message = (e.getMessage() != null) ? e.getMessage() : "not found";
            this.code = HttpStatus.BAD_REQUEST.value();
            this.status = HttpStatus.BAD_REQUEST;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else if (e instanceof ConstraintViolationException) {
            this.message = (e.getMessage() != null) ? e.getMessage() : "constraint";
            this.code = HttpStatus.CONFLICT.value();
            this.status = HttpStatus.CONFLICT;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else if (e instanceof TransactionSystemException) {
            this.message = (e.getCause() != null) ? e.getCause().getMessage() : "transaction error";
            this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
            this.status = HttpStatus.INTERNAL_SERVER_ERROR;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else if (e instanceof PaginationSortingException) {
            this.message = (e.getCause() != null) ? e.getCause().getMessage() : "Invalid sort direction";
            this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
            this.status = HttpStatus.INTERNAL_SERVER_ERROR;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else if (e instanceof NoAccessRightsException) {
            this.message = (e.getCause() != null) ? e.getCause().getMessage() : "No access rights";
            this.code = HttpStatus.FORBIDDEN.value();
            this.status = HttpStatus.FORBIDDEN;
            this.trace = ExceptionUtils.getStackTrace(e);
        } else {
            this.message = e.getMessage();
            this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
            this.status = HttpStatus.INTERNAL_SERVER_ERROR;
            this.trace = ExceptionUtils.getStackTrace(e);
        }
        e.printStackTrace();
    }

}

